#!/bin/sh -x
ARTIFAC_USER="ganesh"

echo "===================Building for iOS=========================="
rm -rf ios/build/
rm -rf ./node_modules
yarn install --ignore-engines

echo "===================Opening workspace=========================="
#open ios/moonwalk.xcworkspace/

pushd .
cd ios
echo "Current Path $(pwd)..."

#export GEM_HOME=~/.gem
export FL_UNLOCK_KEYCHAIN_PASSWORD=$BUILD_PASSWORD
export FL_UNLOCK_KEYCHAIN_SET_DEFAULT="/Users/hawk/Library/Keychains/login.keychain-db"
#bundle install
#bundle update fastlane

if [ -e mookwalk.xcarchive ]
then
  rm -Rf mookwalk.xcarchive
fi

if [ -e moonwalk.app ]
then
  rm -Rf moonwalk.app
fi
pod update

cd ..
echo "Current Path $(pwd)..."
cd ios
echo "Current Path $(pwd)..."

bundle exec fastlane beta
appcenter distribute release --app Pointzi/MoonWalk-2022_Prod_rn_fbreact --file MoonWalk-2022.ipa --group "Collaborators"

# xcodebuild -quiet -workspace moonwalk.xcworkspace -scheme moonwalk -archivePath moonwalk.xcarchive archive
xcodebuild -quiet -workspace moonwalk.xcworkspace -scheme moonwalk  -destination 'generic/platform=iOS Simulator' -configuration Release -archivePath build/moonwalk.xcarchive archive

rm -Rf build/moonwalk.app
mv build/moonwalk.xcarchive/Products/Applications/moonwalk.app build/moonwalk.app
zip -r build/moonwalk.zip build/moonwalk.app

/usr/local/opt/curl/bin/curl --user $ARTIFACTORY_USER:$ARTIFACTORY_PASSWORD -T build/Moonwalk.zip -X PUT \
    "https://repo.pointzi.com/sdk/pointzi/ios/bdd/prod_moonwalk/rn_fbreact/Moonwalk.zip"
