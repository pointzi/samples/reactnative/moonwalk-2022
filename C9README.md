# Moonwalk
React Native app cloned from [illu](https://github.com/Illu/moonwalk). This is the main app
used in react native testing for Contextual SDK.


### Build Instructions
- make sure to follow contextual integration steps on our [docs](https://dashboard.pointzi.com/docs/sdks/android/integration/)
- after a successful build upload the app to AppCenter
- verify test app meets [QA testing requirements](https://streethawk.atlassian.net/wiki/spaces/PPD/pages/1900707864/SDK+Testing+Requirements)